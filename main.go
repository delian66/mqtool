package main

import (
	"flag"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"math/rand"
	"os"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(c int) string {
	b := make([]byte, c)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

/*
Options:
 [-help]                      Display help
 [-a pub|sub]                 Action pub (publish) or sub (subscribe)
 [-m <message>]               Payload to send
 [-n <number>]                Number of messages to send or receive
 [-q 0|1|2]                   Quality of Service
 [-clean]                     CleanSession (true if -clean is present)
 [-id <clientid>]             CliendID
 [-user <user>]               User
 [-password <password>]       Password
 [-broker <uri>]              Broker URI
 [-topic <topic>]             Topic
 [-store <path>]              Store Directory
*/
const LOBBY_CHANNEL_NAME = "lobby"

func main() {
	verbose := flag.Bool("verbose", false, "Set verbose flag (default false)")
	topic := flag.String("topic", "", "The topic name to/from which to publish/subscribe")
	broker := flag.String("broker", "tcp://127.0.0.1:1883", "The broker URI. ex: tcp://10.10.1.1:1883")
	password := flag.String("password", "", "The password (optional)")
	user := flag.String("user", "", "The User (optional)")
	id := flag.String("id", fmt.Sprintf("mqtool_%s", randomString(6)), "The ClientID (optional)")
	cleansess := flag.Bool("clean", false, "Set Clean Session (default false)")
	qos := flag.Int("qos", 0, "The Quality of Service 0,1,2 (default 0)")
	num := flag.Int("num", 1, "The number of messages to publish or subscribe (default 1)")
	payload := flag.String("message", "", "The message text to publish (default empty)")
	action := flag.String("action", "", "Action publish or subscribe (required)")
	store := flag.String("store", ":memory:", "The Store Directory (default use memory store)")
	flag.Parse()
	if *action != "pub" && *action != "sub" {
		fmt.Println("Invalid setting for -action, must be pub or sub")
		return
	}
	if *topic == "" {
		fmt.Println("Invalid setting for -topic, must not be empty")
		return
	}
	if *verbose {
		fmt.Printf("Sample Info:\n")
		fmt.Printf("\taction:    %s\n", *action)
		fmt.Printf("\tbroker:    %s\n", *broker)
		fmt.Printf("\tclientid:  %s\n", *id)
		fmt.Printf("\tuser:      %s\n", *user)
		fmt.Printf("\tpassword:  %s\n", *password)
		fmt.Printf("\ttopic:     %s\n", *topic)
		fmt.Printf("\tmessage:   %s\n", *payload)
		fmt.Printf("\tqos:       %d\n", *qos)
		fmt.Printf("\tcleansess: %v\n", *cleansess)
		fmt.Printf("\tnum:       %d\n", *num)
		fmt.Printf("\tstore:     %s\n", *store)
	}
	//////////////////////////////////////////////////////
	choke := make(chan [2]string)
	opts := MQTT.NewClientOptions()
	opts.SetProtocolVersion(4) // MQTT 3.1.1
	opts.SetWill(LOBBY_CHANNEL_NAME, fmt.Sprintf("LEFT %s", *id), 0, false)
	opts.SetOrderMatters(false)
	opts.AddBroker(*broker)
	opts.SetClientID(*id)
	opts.SetUsername(*user)
	opts.SetPassword(*password)
	opts.SetCleanSession(*cleansess)
	if *store != ":memory:" {
		opts.SetStore(MQTT.NewFileStore(*store))
	}
	opts.SetDefaultPublishHandler(func(client MQTT.Client, msg MQTT.Message) {
		choke <- [2]string{msg.Topic(), string(msg.Payload())}
	})
	if *action == "pub" {
		client := MQTT.NewClient(opts)
		token := client.Connect()
		token.Wait()
		if token.Error() != nil {
			panic(token.Error())
		}
		fmt.Println("Sample Publisher Started")
		for i := 0; i < *num; i++ {
			fmt.Printf("---- doing publish i: %6d ----\n", i)
			token := client.Publish(*topic, byte(*qos), false, *payload)
			token.Wait()
		}
		client.Disconnect(250)
		fmt.Println("Sample Publisher Disconnected")
	} else {
		receiveCount := 0
		client := MQTT.NewClient(opts)
		if token := client.Connect(); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
		lobby_token := client.Publish(LOBBY_CHANNEL_NAME, 0, false, fmt.Sprintf("JOINED %s", *id))
		lobby_token.Wait()
		////////////////////////////////////////////////////////////////////////
		token := client.Subscribe(*topic, byte(*qos), func(client MQTT.Client, msg MQTT.Message) {
			choke <- [2]string{msg.Topic(), string(msg.Payload())}
		})
		token.Wait()
		if token.Error() != nil {
			fmt.Println(token.Error())
			os.Exit(1)
		}
		////////////////////////////////////////////////////////////////////////
		for receiveCount < *num {
			incoming := <-choke
			fmt.Printf("RECEIVED TOPIC: %s MESSAGE: %s\n", incoming[0], incoming[1])
			receiveCount++
		}
		client.Disconnect(250)
		fmt.Println("Sample Subscriber Disconnected")
	}
	os.Exit(0)
}
